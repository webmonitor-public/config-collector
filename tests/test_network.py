import os
import unittest 
from flask import Flask, jsonify, current_app
from flask_testing import TestCase,LiveServerTestCase
import urllib.request as urllib2
import sys
import json
import unittest
sys.path.append('../')
import app
import api.network
INTERFACE = '{5C38E4C1-D303-43D6-A88A-3582DC595160}'

class NetworkTestCase(unittest.TestCase):

    # Start initialization logic 
    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        with app.app.app_context():
            cls.test_app = current_app.test_client()
            cls.inter = app.get_interface_info(INTERFACE).get_json()
        pass

    # clean up logic for the test suite declared in the test module
    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass 
    # Get Network Test
    def test_get_network(self):
        with app.app.app_context(): 
            self.interfaces_ = app.get_interface_info(INTERFACE).get_json()
            # MOCK
            interfaces_mock = ['gateway', 'ipaddr', 'netmask']
            self.assertListEqual(interfaces_mock, list(self.interfaces_.keys()))
    # Set Network Static IP 
    def test_set_static_ip(self):
        self.static_set_ip = api.network.set_static_ip(INTERFACE,"1.1.1.1","255.255.255.0","1.1.1.2",nameservers = ["1.1.1.1"])
        print("TEST SET IP",self.static_set_ip)
        # self.assertEqual(response.code, 200)
    def test_set_dhcp(self):
        self.dhcp_set = api.network.set_dhcp(INTERFACE)
        print("TEST SET DHCP",self.dhcp_set)

if __name__ == '__main__':
    unittest.main(verbosity=2)

