from flask import Flask, jsonify, request, redirect
from flask_restful import Api
from flask_cors import CORS
import time
from threading import Thread
try:
    from .api import network, checkup, dhcpcd, utils, service, tests
except Exception:
    from api import network, checkup, dhcpcd, utils, service, tests

app = Flask(__name__, static_url_path='/static')
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.config['JSON_AS_ASCII'] = False
api = Api(app)
CORS(app)

def reboot():
    print("REBOOTING...")
    time.sleep(5)
    dhcpcd.reboot_pct()

@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route("/api/interfaces")
def get_interfaces():
    """
        Returns : list of interfaces
    """
    try:
        return jsonify(network.get_interfaces())
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "message":"type error: " + str(e),
            "status": "error"
        })

@app.route("/api/interfaces/<interface>")
def get_interface_info(interface):
    """
        Return Interface info
        Requirement : interface
        Returns : { dhcp, ipaddress, mask, gateway, nameservers }
    """
    try:
        interface_ = network.get_interface(interface)
        return jsonify(interface_)
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "message":"type error: " + str(e),
            "status": "error"
        })

@app.route("/api/interfaces/<interface>", methods=["POST"])
def set_ip(interface):
    """
        Set Scout/Collector Ip : [ Static or DHCP ]
        Requirement : interface, 
                      Static config : {ip, mask, gateway=None, nameservers=[] }
                      DHCP config : dhcp = true
    """
    try:
        data_sent = request.json
        if(data_sent['dhcp']):
            set_dhcp = network.set_dhcp(interface)
            interface__ = get_interface_info(interface).get_json()
            Thread(target=reboot).start()
            return jsonify({
                "message":"DHCP set successfully!",
                "status": "success",
                "interface": interface__
            })
        else:
            nameservers = data_sent['nameservers']
            if '\n' in nameservers:
                nameservers = nameservers.split('\n')
            static_ip = network.set_static_ip(interface, data_sent['ipaddr'], data_sent['netmask'], data_sent['gateway'], nameservers)
            Thread(target=reboot).start()
            return jsonify({
                "message":"Static IP set successfully!",
                "status": "success",
                "interface": static_ip
            })
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "message":"type error: " + str(e),
            "status": "error"
        })

@app.route("/api/tests")
def get_all_tests():
    """
        Returns all the tests available
    """
    try:
        tests_ = checkup.get_test()
        return jsonify({
            "status":"success",
            "message":"Tests fetched successfully",
            "tests":tests_
        })
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "message":"type error: " + str(e),
            "status": "error"
        })

@app.route("/api/tests", methods=["POST"])
def run_tests():
    """
        Run Test(s) : [ Single test or all tests]
        Requirement : type { run_all or testid}
        Return : tests_results
    """
    try:
        data_sent = request.json
        if(data_sent['type']=="run_all"):
            # print("RUN ALL TESTS",data_sent)
            checkup.run_all()
            return jsonify({
                "status":"success",
                "message":"Tests fetched successfully",
                "tests_results": "Done"
            })
        else:
            # print("SINGLE TEST RUN",data_sent)
            test_ = checkup.run(data_sent['testid'])
            print("RETURN TEST RESULT .:.",data_sent['testid'] ,test_)
            if(test_):
                return jsonify({
                    "status":"success",
                    "message":"Test executed successfully",
                    "test_results": test_
                })
            else:
                return jsonify({
                    "status":"error",
                    "message":"Test failed",
                    "test_results": test_
                })
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "status": "error",
            "message":"Type error: " + str(e)
        })

@app.route("/api/service/<service_name>/<action>")
def execute_action_on_service(service_name, action):
    actions = {
        'restart': service.restart,
        'stop': service.stop,
        'start': service.start
    }
    try:
        actions[action](service_name)
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "status": "error",
            "message":"Type error: " + str(e)
        })
    return jsonify({'status': True, 'message': 'service %s %sed' % (service_name, action)})

# Compatible routes with older api.py
@app.route("/status")
def do_get_status():
    interface = network.get_interfaces()[0]
    try:
        interface_data = network.get_interface(interface)
        image_version = utils.read_file("/var/www/portal/VERSION")
        status = interface_data
        status['image_version'] = image_version
        return jsonify(status)
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "status": "error",
            "message":"Type error: " + str(e)
        })

@app.route("/check")
def do_check():
    try:
        return jsonify(tests.run_all_tests())
    except Exception as e:
        print("type error: " + str(e))
        return jsonify({
            "status": "error",
            "message":"Type error: " + str(e)
        })

# Routes for Captive Portal on Android and iOS
@app.route("/generate_204")
@app.route("/hotspot-detect.html")
def do_captive_portal():
    return redirect("http://webmonitor.config/", code=302)

# Serve static files (if exists) or redirect all routes that doesn't mapped above for /
@app.route('/<path:path>')
def send_js(path):
    try:
        return app.send_static_file(path)
    except:
        return redirect("http://webmonitor.config/", code=302)

if __name__ == '__main__':
    from waitress import serve
    serve(app, host='0.0.0.0', port=80)
