import subprocess

from . import log

def process_run(command):
    """Run command on OS.

    Requires Python >= 3.5. Uses subprocess.
    """
    if isinstance(command, str):
        command = command.split(" ")

    return subprocess.run(command, check=True)

def manager(name, command="status", sys=None):
    """Runs service manager for the informed service."""
    ctl = {
        "systemd": ("systemctl", command, name),
        "systemctl": ("systemctl", command, name),
        "init": ("service", name, command),
        "init_script": ("service", name, command),
        "service": ("service", name, command),
    }

    if sys:
        try:
            cmd = ctl[sys]
        except KeyError:
            raise NameError(
                "{sys} is not a valid System Manager.".format(sys=sys)
            )
    else:
        cmd = ctl["init_script"]

    log.debug("Running {command}".format(command=" ".join(cmd)))
    return process_run(cmd)

def reboot():
    """Reboots entire system."""
    process_run("reboot now")

def start(name):
    """Starts informed service."""
    manager(name, "start")

def stop(name):
    """Stops informed service."""
    manager(name, "stop")

def restart(name):
    """Restarts informed service."""
    manager(name, "restart")
