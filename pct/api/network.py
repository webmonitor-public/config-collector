# 3rd party
import netifaces
import dns.resolver
# local
from . import dhcpcd

def _read_resolvconf(filename):
   dnsservers = []
   f = open(filename, 'r')
   for line in f.readlines():
       if line.startswith('nameserver'):
           values = line.split(' ')
           dnsservers.append(values[1].strip())
   return dnsservers

def get_ip(interface, version=4):
    """Returns IP address, netmask and gateway for the interface.

    Returns a dict with only the first IP address for the specified interface.
    Returns an empty string on gateway field if no gateway is found for that
    interface.

    Args:
        interface: A string with the interface name.
        version: An optional int for the desired Internet Protocol Version
            (supports IPv4 and IPv6 only).

    Returns:
        A dict with the IP data for the interface. For example:

        {'ipaddr':'192.168.0.99',
         'netmask':'255.255.255.0',
         'gateway':'192.168.0.1', }

    Raises:
        SomeError: An error occurred accessing the ...
    """

    interface_type = {4: netifaces.AF_INET, 6: netifaces.AF_INET6}.get(version)

    # Retrieves the first IP
    try:
        ip_list = netifaces.ifaddresses(interface)[interface_type]
        ip = ip_list[0]
    except KeyError:
        raise KeyError(
            "Protocol IPv{ipversion} not found on interface {interface}.".format(
                ipversion=version, interface=interface
            )
        )
    except ValueError:
        raise ValueError("Interface {} not found.".format(interface))

    # Retrieves gateways for each interface
    gw_list = netifaces.gateways().get(interface_type, {})
    gateways = {
        ifc: gip for (gip, ifc, default) in sorted(gw_list, key=lambda x: x[2])
    }
    gw = gateways.get(interface, "")
    macaddr = netifaces.ifaddresses(interface)[netifaces.AF_LINK][0]['addr']
    return {"ipaddr": ip["addr"], "netmask": ip["netmask"], "gateway": gw, "macaddr": macaddr }

def set_static_ip(interface, ip, mask, gateway=None, nameservers=[]):
    static_args = {"ipaddr": ip, "mask": mask}
    if gateway:
        static_args["gateway"] = gateway
    if nameservers:
        static_args["nameservers"] = nameservers
    return dhcpcd.set_interface(interface, **static_args)

def get_dhcp(interface):
    return dhcpcd.is_dhcp(interface)

def set_dhcp(interface):
    return dhcpcd.set_interface(interface, dhcp=True)

def get_nameservers():
    resolv = dns.resolver.get_default_resolver()
    if '127.0.0.1' in resolv.nameservers:
        return _read_resolvconf('/run/dnsmasq/resolv.conf')
    return resolv.nameservers

def get_interface(interface):
    interface_data = get_ip(interface)
    interface_data['dhcp'] = get_dhcp(interface)
    interface_data['nameservers'] = get_nameservers()
    return interface_data

def get_interfaces():
    interface_list = netifaces.interfaces()
    return list(filter(lambda interface: interface != 'wlan0' and interface != 'lo', interface_list))
