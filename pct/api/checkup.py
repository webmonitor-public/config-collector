import socket
from scapy import route
from scapy.sendrecv import sr
from scapy.layers.inet import IP, ICMP, TCP, UDP

from . import log 
from . import network 
from . import utils 

# config
# EXTERNAL_INTERFACE = "{5C38E4C1-D303-43D6-A88A-3582DC595160}"
EXTERNAL_INTERFACE = "eth0"
CHECKUP_TEST = [
    {
        "testid": "netconn",
        "name": "Network Connection",
        "descr": "Checks network interface connection.",
        "command": "is_connected",
        "status": "pause",
    },
    {
        "testid": "gateway",
        "name": "Gateway",
        "descr": "Checks if gateway responds to ping.",
        "command": "ping_gw",
        "status": "pause",
    },
    {
        "testid": "dns",
        "name": "DNS name resolution",
        "descr": "Checks if nameserver responds.",
        "command": "dns_resolv",
        "args": "aws.amazon.com",
        "status": "pause",
    },
    {
        "testid": "cloud",
        "name": "WebMonitor Cloud",
        "descr": "Checks connection to the WebMonitor cloud.",
        "command": "cloud_check",
        "status": "pause",
    },
]


def is_connected(interface=None):
    if interface is None:
        interface = EXTERNAL_INTERFACE
    try:
        return bool(network.get_ip(interface, version=4))
    except KeyError:
        return False
    # except Exception as e:
    #     print("Is connected Test Error: " + str(e))
    #     message = str(e)
    #     return ({"message": message, "status": "error"})

def ping_icmp(target, timeout=2):
    ans, unans = sr(IP(dst=target) / ICMP(), timeout=timeout)
    return any(True for s, r in ans if r.src == target)

def dns_resolv(hostname):
    try:
        return bool(socket.gethostbyname(hostname))
    except socket.gaierror:
        return False

def ping_gw():
    gw = network.get_ip(EXTERNAL_INTERFACE)["gateway"]
    return ping_icmp(gw)

def ping_tcp(target, port=None, timeout=2):
    """Tests if target answers to TCP SYN.

    If no port is specified, only checks if there is a response from the host.
    If a port a specified, validate if the port is open (answer with SYN-ACK).
    """
    if port is None:
        ans, unans = sr(IP(dst=target) / TCP(flags="S"), timeout=timeout)
        return bool(ans)
    else:
        ans, unans = sr(
            IP(dst=target) / TCP(dport=port, flags="S"), timeout=timeout
        )
        # Example of use from https://markdown222.readthedocs.io/en/latest/usage.html
        return any((r.haslayer(TCP) and (r.getlayer(TCP).flags & 2)) for s, r in ans)

def ping_udp(target, port):
    raise NotImplementedError("Function not implemented")

def conn_port(target, port, protocol="TCP"):
    ping_l3 = {"TCP": ping_tcp, "UDP": ping_udp}
    try:
        return ping_l3[protocol](target, port)
    except KeyError:
        raise ValueError(
            "Protocol {protocol} not supported!".format(protocol=protocol)
        )

def cloud_check():
    services = [
        ("gc.webmonitor.global", 4505),
        ("gc.webmonitor.global", 4506),
        ("sm-srv01.webmonitor.global", 10051),
    ]
    return all(conn_port(*s) for s in services)

def exec_cmd(name, arguments=None):
    # Allowed commands
    COMMAND = {
        # "get_ip": network.get_ip,
        "is_connected": is_connected,
        "ping_gw": ping_gw,
        "dns_resolv": dns_resolv,
        "cloud_check": cloud_check,
    }

    try:
        func = COMMAND[name]
    except KeyError:
        raise RuntimeError('Command "{cmd}" not found!'.format(cmd=name))
    finally:
        log.debug("Command: {name} -> {func}".format(name=name, func=func))

    args = []
    kwargs = {}
    if arguments is None:
        pass
    elif isinstance(arguments, dict):
        kwargs = arguments
    elif isinstance(arguments, (list, tuple)):
        args = arguments
    elif isinstance(arguments, str):
        args = utils.listify(arguments)
    else:
        args = [arguments]

    log.debug("Arguments: {}".format(kwargs or args))
    return func(*args, **kwargs)


def _export(test):
    fields = [("name", "name"), ("descr", "descr"), ("status", "status")]
    return {field: test[value] for field, value in fields}

def get_test(testid=None):
    """Returns check's definitions."""
    key = lambda x: x["testid"]
    value = _export
    if testid is None:
        # return utils.list2dict_fn(CHECKUP_TEST, key, value)
        return CHECKUP_TEST
    else:
        return utils.list2dict_fn(CHECKUP_TEST, key, value)[testid]

def run(testid):
    try:
        check = utils.list2dict(CHECKUP_TEST, "testid")[testid]
    except KeyError:
        raise RuntimeError('Test "{id}" not found!'.format(id=testid))
    cmd = check["command"]
    args = check.get("args")
    return exec_cmd(cmd, args)

def run_all():
    for check in CHECKUP_TEST:
        run(check["testid"])
