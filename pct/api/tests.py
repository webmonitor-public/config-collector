import salt.client
import salt.config
import dns.resolver
import os
import socket

def get_master():
    conf = salt.config.minion_config('/etc/salt/minion')
    return conf['master']

def test_dns(host):
    try:
       dns_test = dns.resolver.query(host,'A')
       return 'OK'
    except:
       return 'FALHA'

def test_ping(host):
    tping = os.system("ping -c 1 %s > /dev/null 2>&1" % host)
    if tping ==0:
       return 'OK'
    else:
       return 'FALHA'

def test_port(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(5)
    result = sock.connect_ex((ip, port))
    sock.close()
    if result == 0:
       return 'OK'
    else:
       return 'FALHA'

def test_zbx(porta):
    try:
        caller = salt.client.Caller()
        IPZS = caller.cmd('pillar.get', 'zabbix-proxy:server')
        statusportazs = test_port(IPZS, porta)
        return statusportazs
    except:
        return 'FALHA'

def run_all_tests():
    IP = get_master()
    PORTA1 = 4505
    PORTA2 = 4506
    PORTAZS = 10051
    data = {}
    statusdns = test_dns('terra.com.br')
    statusdnswbm = test_dns('gc.webmonitor.global')
    pingstatus = test_ping('8.8.8.8')
    pingstatuswbm = test_ping('52.25.101.153')
    statusporta1 = test_port(IP, PORTA1)
    statusporta2 = test_port(IP, PORTA2)
    statusportazs = test_zbx(PORTAZS)
    data['Teste de ping'] = pingstatus
    data['Teste de ping Webmonitor'] = pingstatuswbm
    data['Resolução de nome'] = statusdns
    data['Resolução de nome Webmonitor'] = statusdnswbm
    data['Teste na porta '+str(PORTA1)] = statusporta1
    data['Teste na porta '+str(PORTA2)] = statusporta2
    data['Teste na porta '+str(PORTAZS)] = statusportazs
    return data
