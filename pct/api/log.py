def err(msg):
    print("ERROR: {message}".format(message=msg))

def info(msg):
    print("INFO: {message}".format(message=msg))

def debug(msg):
    print("DEBUG: {message}".format(message=msg))
