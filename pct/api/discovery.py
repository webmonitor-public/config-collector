import ipaddress
import netaddr
# from scapy.all import srp, Ether, ARP
import scapy.all as scapy
import network_config

def arp_discovery(target):
    """ARP ping

    Args:
        target: A string with the IP or network to be discovered.

    Returns:
        A list of tuples with 3 values each: IP, MAC address of IP, MAC address 
        who answered to ARP.
    """

    ans, unans = scapy.srp(
        scapy.Ether(dst="ff:ff:ff:ff:ff:ff") / scapy.ARP(pdst=target), timeout=2
    )
    return [(r.psrc, r.hwsrc, r.src) for s, r in ans]

def get_mac_org(mac):
    """Get the organization name for the informed MAC address.

    Args:
        mac: A string with the MAC address.

    Returns:
        A string with the organization name.
    """
    try:
        return netaddr.EUI(mac).oui.registration().org
    except netaddr.NotRegisteredError:
        return "N/A"

def local_network(interface):
    local_ip = network_config.get_ip(interface)
    network = ipaddress.ip_interface(
        local_ip["ipaddr"] + "/" + local_ip["netmask"]
    ).network

    return [
        (ip, mac, get_mac_org(mac))
        for ip, mac, src in arp_discovery(str(network))
    ]
