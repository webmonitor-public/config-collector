#!/usr/bin/python3
import netifaces

def get_ip(interface, version=4):
    """Returns the first IP address, netmask and gateway for the interface.

    Returns a dict with only the first IP address for the specified interface.
    Returns an empty string on gateway field if no gateway is found for that
    interface.

    Args:
        interface: A string with the interface name.
        version: An optional int for the desired Internet Protocol Version
            (supports IPv4 and IPv6 only).

    Returns:
        A dict with the IP data for the interface. For example:

        {'ipaddr':'192.168.0.99',
         'netmask':'255.255.255.0',
         'gateway':'192.168.0.1', }

    Raises:
        SomeError: An error occurred accessing the ...
    """

    interface_type = {4: netifaces.AF_INET, 6: netifaces.AF_INET6}.get(version)

    # Retrieves the first IP
    try:
        ip_list = netifaces.ifaddresses(interface)[interface_type]
        ip = ip_list[0]
    except KeyError:
        raise KeyError(
            "Protocol IPv{} not found on interface {}.".format(
                version, interface
            )
        )
    except ValueError:
        raise ValueError("Interface {} not found.".format(interface))

    # Retrieves gateways for each interface
    gw_list = netifaces.gateways().get(interface_type, {})
    gateways = {
        ifc: gip for (gip, ifc, default) in sorted(gw_list, key=lambda x: x[2])
    }
    gw = gateways.get(interface, "")
    return {"ipaddr": ip["addr"], "netmask": ip["netmask"], "gateway": gw}

# ------------------------------------------------------------------------------
# ***   ATTENTION!    Under active develpment!    ***
# ------------------------------------------------------------------------------
def set_static_ip(interface, ip, mask, gateway=None, nameservers=[]):
    # 1. access interface (exists or is valid)
    # 2. determine ip version
    # 3. determine network configuration in use and call set_ip() accordingly
    #     (network interfaces, DHCPCD, NetworkManager, systemd.networkd, etc.)
    return

# ------------------------------------------------------------------------------
# Extra functions (should be exported to other source file)
# ------------------------------------------------------------------------------
from scapy.all import srp, Ether, ARP
import netaddr
import ipaddress

def arp_discovery(target):
    """ARP ping

    Args:
        target: A string with the IP or network to be discovered.

    Returns:
        A list of tuples with 3 values each: IP, MAC address of IP, MAC address 
        who answered to ARP.
    """

    ans, unans = srp(
        Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=target), timeout=2
    )
    return [(r.psrc, r.hwsrc, r.src) for s, r in ans]

def get_mac_org(mac):
    """Get the organization name who 

    Args:
        target: A string with the IP or network to be discovered.

    Returns:
        A list of tuples with 3 values each: IP, MAC address of IP, MAC address 
        who answered to ARP.
    """
    try:
        return netaddr.EUI(mac).oui.registration().org
    except netaddr.NotRegisteredError:
        return "N/A"

def local_network(interface):
    local_ip = get_ip(interface)
    local_network = ipaddress.ip_interface(
        local_ip["ipaddr"] + "/" + local_ip["netmask"]
    ).network

    # return [node(ip, mac) for ip, mac, src in arp_discovery(str(local_network))]
    return [
        (ip, mac, get_mac_org(mac))
        for ip, mac, src in arp_discovery(str(local_network))
    ]
# ------------------------------------------------------------------------------
