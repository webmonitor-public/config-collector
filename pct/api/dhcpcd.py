# 3rd party
import ipaddress
from collections import defaultdict
# local
from . import utils
from . import service
from . import config
from . import log

CONFIG_FILE = config.DHCPCD_CONF

NEWLINE = "\n"

BASE_CONF = """hostname
duid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option ntp_servers
option interface_mtu
require dhcp_server_identifier
slaac private
# nohook lookup-hostname

interface wlan0
static ip_address=172.31.31.1/24
"""

def valid_interface_ip(ipaddr, netmask):
    """Validates the arguments as IP address and netmask.

    Return an IPv4Address or IPv6Address object depending on the IP address
    passed as argument. Return None if ipaddr does not represent a valid IPv4
    or IPv6 address.
    """
    # RFC numbers where the reserved addresses are defined (by IP version).
    rfc = {4: 6890, 6: 4291}
    ip = ipaddress.ip_interface(ipaddr + "/" + netmask)
    if ip.is_loopback or ip.is_multicast or ip.is_reserved or ip.is_unspecified:
        raise ipaddress.AddressValueError(
            "Address {ip} is not valid! See RFC{rfc} for more details.".format(
                ip=ip, rfc=rfc.get(ip.version, "")
            )
        )
    return ip

def valid_ip(ipaddr):
    """Validates if the argument is a valid IP address.

    Return an IPv4Address or IPv6Address object depending on the IP address
    passed as argument. Return None if ipaddr does not represent a valid IPv4
    or IPv6 address.
    """
    try:
        ip = ipaddress.ip_address(ipaddr)
    except ValueError:
        ip = None
    return ip

def format_line(option, value="", disable=False):
    prefix = "#" if disable else ""
    suffix = ""
    line = " ".join((prefix, option, value, suffix))
    return line.strip()

def config_block(config):
    return [format_line(*c) for c in config]

def config(block):
    """Creates the configuration text to be saved on dhcpcd.conf.
    """
    final_config = BASE_CONF

    if block:
        final_config += NEWLINE
        if isinstance(block, str):
            final_config += block
        else:
            final_config += NEWLINE.join(block)

    return final_config

def interface_block(interface, ipaddr, mask, *, gateway=None, nameservers=None):
    # result = [("interface", interface)]
    result = [format_line(option="interface", value=interface)]

    ip = valid_interface_ip(ipaddr, mask)
    result.append(
        format_line(option="static", value="ip_address={ip}".format(ip=ip))
    )

    if gateway:
        gw = ipaddress.ip_address(gateway)
        if gw not in ip.network:
            raise ValueError(
                "Gateway {} is NOT on the same network of {}.".format(
                    gw, ip.network
                )
            )
        result.append(
            format_line(option="static", value="routers={gw}".format(gw=gw))
        )

    if nameservers:
        nameservers = utils.listify(nameservers, clean=True)
        ns_list = [ipaddress.ip_address(ns) for ns in nameservers]
        resolvers = " ".join(nameservers)
        result.append(
            format_line(
                option="static",
                value="domain_name_servers={ns}".format(ns=resolvers),
            )
        )

    return result

def set_interface(interface, **kwargs):
    """Sets IP configuration for interface.

    """
    # print("SETTING IP")
    if kwargs:
        dhcp = utils.arg_name("dhcp", required=False, **kwargs)
    else:
        # If no extra arguments are received, use auto mode (DHCP).
        dhcp = True

    if dhcp:
        final_config = config("")
    else:
        final_config = config(interface_block(interface, **kwargs))

    log.debug('Saving configuration to "{}".'.format(CONFIG_FILE))
    with open(CONFIG_FILE, "w") as f:
        # print("FINAL CONFIG",final_config)
        f.write(final_config)

    # try:
    #     # service.restart("dhcpcd")
    #     service.reboot()
    #     # pass
    # except FileNotFoundError:
    #     log.err("Service could NOT be restarted!")
    #     # raise

    return final_config

def reboot_pct():
    try:
        # service.restart("dhcpcd")
        service.reboot()
        # pass
    except FileNotFoundError:
        log.err("Service could NOT be restarted!")
        # raise

def parse_config(text):
    if isinstance(text, str):
        lines = text.splitlines()
    valid_lines = utils.cleanList(lines, False)
    valid_lines = [l for l in valid_lines if l[0] != "#"]

    block = "main"
    result = defaultdict(list)
    for l in valid_lines:
        option, _, value = l.partition(" ")
        if option == "interface":
            block = (option, value)
        else:
            ci = (option, value) if value else (option,)
            result[block].append(ci)
    return result

def is_dhcp(interface):
    static = False
    try:
        with open(CONFIG_FILE, "r") as f:
            file_data = f.read()

        cblock = parse_config(file_data)
        static = any(o == "static" for o, v in cblock[("interface", interface)])
    # if file doesn't exists assume that is DHCP
    except:
        return True
    finally:
        return not static
