import psutil

def list2dict_fn(list_in, fn_key, fn_value=lambda v: v):
    return {fn_key(item): fn_value(item) for item in list_in}

def list2dict(dictList, key):
    return list2dict_fn(dictList, lambda x: x[key])

def intOrNone(number):
    try:
        return int(number)
    except (ValueError, TypeError):
        return None

def cleanTuple(dirtyTuple):
    return tuple(clean(a) for a in dirtyTuple)

def cleanList(dirtyList, keep_empty=None):
    return list(clean(a) for a in dirtyList if any([keep_empty, a]))

def clean(dirtyObj, ignoreEmpty=None):
    if isinstance(dirtyObj, tuple):
        return cleanTuple(dirtyObj)
    elif isinstance(dirtyObj, list):
        return cleanList(dirtyObj, ignoreEmpty)
    elif isinstance(dirtyObj, str):
        return dirtyObj.strip()
    else:
        return dirtyObj

def listify(arg, separator=",", clean=False):
    if isinstance(arg, str):
        result = arg.split(separator)
    else:
        try:
            result = [a for a in arg]
        except TypeError:
            result = [arg]

    return cleanList(result) if clean else result

# Copied from psutil documentation
# https://psutil.readthedocs.io/en/latest/#find-process-by-name
def find_procs_by_name(name):
    "Return a list of processes matching 'name'."
    ls = []
    for p in psutil.process_iter(attrs=["name"]):
        if p.info["name"] == name:
            ls.append(p)
    return ls

def process_is_running(*args, **kwargs):
    """Checks if process informed as argument is currently running.
    """
    if args or kwargs:
        try:
            pid = int(args[0] or kwargs.get("pid"))
        except (ValueError, TypeError):
            pid = None

        if pid:
            return psutil.pid_exists(pid)
        else:
            name = args[0] or kwargs.get("name")
            return bool(find_procs_by_name(name))

    else:  # if not args and not kwargs:
        raise TypeError("Missing argument. Must inform pid or name.")

def arg_name(name, *, required=True, **kwargs):
    if required:
        try:
            return kwargs[name]
        except KeyError:
            raise TypeError(
                "Missing required argument: '{arg}'".format(arg=name)
            )
    else:
        return kwargs.get(name)

def read_file(filename):
    try:
        value = open(filename).read().replace('\n','')
        return value
    except:
        return '0.0'
