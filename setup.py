from setuptools import setup

setup(name='pct',
      version='0.3',
      description='Webmonitor Configuration package',
      url='https://gitlab.com/webmonitor-public/config-collector.git',
      author='WebMonitor',
      author_email='admin@unirede.net',
      license='MIT',
      packages=['pct'],
      install_requires=['scapy-python3','netaddr','psutil','netifaces', 'dnspython','flask','flask_restful', 'flask_cors', 'waitress'],
      include_package_data=True,
      zip_safe=False)
